# Page swapping #
## Project created as an assignment for *Object-Oriented Programming* course ##

### Task ###
The purpose of this assignment is to implement package `wymiana` simulating page swapping in virtual memory.

Package should contain `Odwołania` class implementing swapping mechanism for a given sequence of page requests, a given strategy and a given number of frames.
Class `Odwołania` should have:

*  public constructor:

`public Odwołania(int[] tab, String s, int rozmiar)`

where `tab` is a sequence of pages’ numbers, `s` is an abbreviation of a strategy and `rozmiar` defines the number of available frames. 

* method:

`public void wykonaj()`

which conducts the simulation. 

You should implement the following strategies: OPT, FIFO, LRU and CLOCK.

While `wykonaj` method is being executed, on the standard output should appear information about current actions in the simulation.

### Grade ###
For this assignment I got 9.2 out of 10.0 points.