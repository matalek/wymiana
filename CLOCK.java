// Aleksander Matusiak

package wymiana;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.TreeSet;

public class CLOCK extends Strategia {
    private LinkedList kolejka;
    private HashMap bitElementu; // pamięta, jaki jest bit strony w pamięci
    private TreeSet pamięć;

    public CLOCK(int[] tab, int rozmiar) {
        super(tab, rozmiar);
    }
    
    @Override
    protected void przygotujPamięć() {
        kolejka = new LinkedList();
        pamięć = new TreeSet();
        bitElementu = new HashMap();
    }
    
    @Override
    protected boolean czyZawiera(int el) {
        return pamięć.contains(el);
    }
    
    @Override
    protected void aktualizujPamięć(int el, int index) {
        bitElementu.put(el, (byte) 1);
    }
    
    @Override
    protected int wybierzDoUsunięciaIUsuń() {
        while ((byte) bitElementu.get(kolejka.peek()) == 1) {
            int usunięty = (int) kolejka.remove();
            bitElementu.put(usunięty, (byte) 0);
            kolejka.add(usunięty);
        }
        pamięć.remove(kolejka.peek());
        bitElementu.remove(kolejka.peek());
        return (int) kolejka.remove();
    }

    @Override
    protected void dodaj(int el, int index) {
        pamięć.add(el);
        kolejka.add(el);
        bitElementu.put(el, (byte) 1);
    }

}
