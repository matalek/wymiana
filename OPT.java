// Aleksander Matusiak

package wymiana;

import java.util.HashMap;
import java.util.TreeSet;

public class OPT extends Strategia {
    // na której pozycji jest następne wystąpienie danego elementu
    private int[] następneWystąpienie;
    // pamięta następne wystąpienia stron z pamięci
    private TreeSet kolejkaNastępnychWystąpień;
    private TreeSet pamięć;
    // pamięta elementy z pamięci, do których już nie będzie odwołań
    private TreeSet elementyNiewystępujące;

    public OPT(int[] tab, int rozmiar) {
        super(tab, rozmiar);
    }
    
    @Override
    protected void przygotujPamięć() {
        następneWystąpienie = new int[tab.length];
        kolejkaNastępnychWystąpień = new TreeSet();
        pamięć = new TreeSet();
        elementyNiewystępujące = new TreeSet();
        
        // przygotowanie tablicy następnych wystąpień, przechodząc
        // tablicę od tyłu
        HashMap aktualneNastępneWystąpienie = new HashMap();
        for (int i = tab.length - 1; i >= 0; i--) {
            int el = tab[i];
            if (aktualneNastępneWystąpienie.get(el) == null)
                następneWystąpienie[i] = -1;
            else
                następneWystąpienie[i] = (int) aktualneNastępneWystąpienie.get(el);
            aktualneNastępneWystąpienie.put(el, i);
        }
    }
    
    @Override
    protected boolean czyZawiera(int el) {
        return pamięć.contains(el);
    }
    
    @Override
    protected void aktualizujPamięć(int el, int index) {
        if (następneWystąpienie[index] == -1)
            elementyNiewystępujące.add(el);
        else
            kolejkaNastępnychWystąpień.add(następneWystąpienie[index]);
    }
    
    @Override
    protected int wybierzDoUsunięciaIUsuń() {
        if (!elementyNiewystępujące.isEmpty()) {
            // usuwamy stronę o najmniejszym numerze, do której nie ma odwołań
            int pierwszy = (int) elementyNiewystępujące.pollFirst();
            pamięć.remove(pierwszy);
            return pierwszy;
        } else {
            // usuwamy stronę występującą najpóźniej
            int indeksOstatniego = (int) kolejkaNastępnychWystąpień.pollLast();
            pamięć.remove(tab[indeksOstatniego]);
            return tab[indeksOstatniego];
        }
    }

    @Override
    protected void dodaj(int el, int index) {
        pamięć.add(el);
        if (następneWystąpienie[index] == -1)
            elementyNiewystępujące.add(el);
        else
            kolejkaNastępnychWystąpień.add(następneWystąpienie[index]);
    }
    
}
