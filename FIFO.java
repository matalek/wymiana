// Aleksander Matusiak

package wymiana;

import java.util.LinkedList;
import java.util.TreeSet;

public class FIFO extends Strategia {
    private LinkedList kolejka;
    private TreeSet pamięć;
    
    public FIFO(int[] tab, int rozmiar) {
        super(tab, rozmiar);
    }
    
    @Override
    protected void przygotujPamięć() {
        pamięć = new TreeSet();
        kolejka = new LinkedList(); 
    }
    
    @Override
    protected boolean czyZawiera(int el) {
        return pamięć.contains(el);
    }
    
    @Override
    protected int wybierzDoUsunięciaIUsuń() {
        pamięć.remove(kolejka.getFirst());
        return (int) kolejka.remove();
    }

    @Override
    protected void dodaj(int el, int index) {
        pamięć.add(el);
        kolejka.add(el);
    }  

}
