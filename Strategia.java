// Aleksander Matusiak

package wymiana;

public abstract class Strategia {
    protected int[] tab;
    private final int rozmiar;

    public Strategia(int[] tab, int rozmiar) {
        this.tab = new int[tab.length];
        System.arraycopy(tab, 0, this.tab, 0, tab.length);
        this.rozmiar = rozmiar;
    }
    
    protected abstract void przygotujPamięć();
    
    protected abstract boolean czyZawiera (int el);
    
    protected void aktualizujPamięć(int el, int index) { }
    
    protected abstract int wybierzDoUsunięciaIUsuń();
    
    // wstawia stronę do pamięci
    protected abstract void dodaj(int el, int index);
    
    public void wykonaj() {
        int błędy = 0, doUsunięcia, index = 0, ileWPamięci = 0;
        przygotujPamięć();
        
        for (int el : tab) {
            if (czyZawiera(el))
                aktualizujPamięć(el, index);
            else {
                błędy++;
                if (ileWPamięci == rozmiar)
                    doUsunięcia = wybierzDoUsunięciaIUsuń();
                else {
                    doUsunięcia = 0;
                    ileWPamięci++;
                }
                dodaj(el, index);
                System.out.println("Błąd: " + doUsunięcia + " --> " + el);
            }
            index++;
        }
        
        System.out.println("Odwołania: " + tab.length + ", błędy: " + błędy);
    }

}
