// Aleksander Matusiak

package wymiana;

public class Wymiana {

    public static void main(String[] args) {
        int[] t = { 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4 };
        try {
            Odwołania o2 = new Odwołania(t, "OPT", 1);
            o2.wykonaj();
        } catch (NiepoprawneDaneException e) {
            System.out.println("Niepoprawne dane.");
        }
    }
    
}
