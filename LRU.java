// Aleksander Matusiak

package wymiana;

import java.util.HashMap;
import java.util.TreeSet;

public class LRU extends Strategia {
    // pamięta ostatnie wystąpienia stron z pamięci w kolejności rosnącej
    private TreeSet kolejkaWystąpień;
    private HashMap ostatnieWystąpienieElementu; 

    public LRU(int[] tab, int rozmiar) {
        super(tab, rozmiar);
    }
    
    @Override
    protected void przygotujPamięć() {
        kolejkaWystąpień = new TreeSet();
        ostatnieWystąpienieElementu = new HashMap();
    }
    
    @Override
    protected boolean czyZawiera(int el) {
        return ostatnieWystąpienieElementu.containsKey(el);
    }
    
    @Override
    protected void aktualizujPamięć(int el, int index) {
        kolejkaWystąpień.remove(ostatnieWystąpienieElementu.get(el));
        dodaj(el, index);
    }
    
    @Override
    protected int wybierzDoUsunięciaIUsuń() {
        int indeksPierwszego = (int) kolejkaWystąpień.pollFirst();
        ostatnieWystąpienieElementu.remove(tab[indeksPierwszego]);
        return tab[indeksPierwszego];
    }

    @Override
    protected void dodaj(int el, int index) {
        kolejkaWystąpień.add(index);
        ostatnieWystąpienieElementu.put(el, index);
    }
    
}
