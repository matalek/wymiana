// Aleksander Matusiak

package wymiana;

public class Odwołania {
    private Strategia st;
    private String opisOdwołania;
    
    public Odwołania(int[] tab, String s, int rozmiar) 
            throws NiepoprawneDaneException{
        opisOdwołania = "Strategia: " + s + ", ramki: " + rozmiar;
        switch (s) {
            case "OPT":
                st = new OPT(tab, rozmiar);
                break;
            case "FIFO":
                st = new FIFO(tab, rozmiar);
                break;
            case "LRU":
                st = new LRU(tab, rozmiar);
                break;
            case "CLOCK":
                st = new CLOCK(tab, rozmiar);
                break;
            default:
                throw new NiepoprawneDaneException();
        }
    }
    
    public void wykonaj() {
        System.out.println(opisOdwołania);
        st.wykonaj();
    }
    
}
